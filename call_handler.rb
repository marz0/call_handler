require 'csv'

class CallHandler
  def initialize
    @mutex = Mutex.new
    @start_time = current_float_time
    @time_standard_handling_available = current_float_time
    @incoming_calls = []
    @standard_queue = []
    @callers_handled = []
  end

  def begin_handling
    initiate_logging
    begin
      process_a = Thread.new { check_incoming_calls }
      process_b = Thread.new { handle_next_caller }
      process_c = Thread.new { monitor_standard_queue }
      [process_a, process_b, process_c].each { |p| p.send(:join) }
    ensure
      terminate_logging
    end
  end

  private

  # NOTE: Process A: Adds items to queue
  def check_incoming_calls
    while calls_not_finished
      new_caller = @incoming_calls.first
      add_to_standard_queue(new_caller) if incoming_call_valid(new_caller)
    end
  end

  # NOTE: Process B: Removes items from queue
  def handle_next_caller
    while calls_not_finished
      next_caller = @standard_queue.first
      if next_caller
        handle_call_from_standard_queue(next_caller) if standard_handling_available
        move_to_priority_queue(next_caller) if next_caller[:past_due]
      end
    end
  end

  # NOTE: Process C: Monitors standard queue
  def monitor_standard_queue
    while calls_not_finished
      mark_caller_past_due(caller_past_due[:id]) if caller_past_due
    end
  end

  def mark_caller_past_due(caller_id)
    @mutex.synchronize do
      caller_to_mark = @standard_queue.find { |c| c[:id] == caller_id }
      caller_to_mark[:past_due] = true if caller_to_mark
    end
  end

  def add_to_standard_queue(new_caller)
    @mutex.synchronize do
      @incoming_calls = @incoming_calls.select { |c| c[:id] != new_caller[:id] }
      @standard_queue << {
        id: new_caller[:id],
        time_added_to_queue: current_float_time,
        time_to_handle: new_caller[:time_to_handle]
      }
    end
  end

  def handle_call_from_standard_queue(next_caller)
    @mutex.synchronize do
      @standard_queue = @standard_queue.select { |c| c[:id] != next_caller[:id] }
      @callers_handled << {
        id: next_caller[:id],
        handling_time: current_float_time - next_caller[:time_added_to_queue],
        handling_cost: 1
      }
      @time_standard_handling_available = current_float_time + next_caller[:time_to_handle]
    end
  end

  def move_to_priority_queue(next_caller)
    @mutex.synchronize do
      @standard_queue = @standard_queue.select { |c| c[:id] != next_caller[:id] }
      @callers_handled << {
        id: next_caller[:id],
        handling_time: current_float_time - next_caller[:time_added_to_queue],
        handling_cost: 2
      }
    end
  end

  def incoming_call_valid(new_caller)
    return false if @standard_queue.find {|c| c[:id] == new_caller[:id] }
    elapsed_time = current_float_time - @start_time
    elapsed_time >= new_caller[:time_in]
  end

  def calls_not_finished
    !@incoming_calls.empty?
  end

  def caller_past_due
    @standard_queue.find do |c|
      ((current_float_time - c[:time_added_to_queue]) > 60.0) && !c[:past_due]
    end
  end

  def current_float_time
    Time.now.to_f
  end

  def standard_handling_available
    current_float_time >= @time_standard_handling_available
  end

  def initiate_logging
    import_caller_csv
    create_csv_log
  end

  def terminate_logging
    CSV.open("queue_log_#{@start_time.to_i}.csv", "a+") do |csv|
      @callers_handled.each do |c|
        csv << [c[:id], c[:handling_time], c[:handling_cost]]
      end
    end
  end

  def create_csv_log
    CSV.open("queue_log_#{@start_time.to_i}.csv", 'wb') do |csv|
      csv << ['id', 'handling_time', 'handling_cost']
    end
  end

  def import_caller_csv
    CSV.foreach('GLOBOQueueDataForTest.csv', headers: true) do |row|
      @incoming_calls << {
        id: row[0].to_i,
        time_in: row[1].to_i,
        time_to_handle: row[2].to_i
      }
    end
  end
end

CallHandler.new.begin_handling
